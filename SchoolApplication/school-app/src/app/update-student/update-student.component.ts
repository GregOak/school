import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IRegister } from '../data/imodel';
import { NgbActiveModal, NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { RegisterService } from '../data/register.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-update-student',
  templateUrl: './update-student.component.html',
  styleUrls: ['./update-student.component.scss']
})
export class UpdateStudentComponent implements OnInit {
  closeResult: any;
  myForm: FormGroup;
  students:IRegister[];


  
  constructor(public activeModal: NgbActiveModal,private modlService: NgbModal,private formBuilder: FormBuilder,private toastr: ToastrService,
    private svc:RegisterService,private spinner: NgxSpinnerService
) { }


  ngOnInit() {
    this.createForm();
    this.getStudentsByID();
  }
  open(content) {
    this.modlService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  createForm() {
    this.myForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      dob: ['', [Validators.required, ]],
      gender: ['', [Validators.required, ]],
      grade: ['', [Validators.required, ]],
      schoolCode: ['', [Validators.required]],
      schoolName: ['', [Validators.required]],
    
    });

  }
  submitForm(){

    if (!this.myForm.valid) {
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
    }, 5000);
      return this.toastr.error('Please Fill In The Fields...', 'Error');
    }
    console.log(this.myForm.value);
    this.spinner.show();
    this.svc.updateStudent(localStorage.getItem('studentID'),this.myForm.value).subscribe(data =>{
      console.log(data);
      this.getStudents();
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
        this.modlService.dismissAll();
          }, 5000);
     
      return this.toastr.success('Student Details Saved Successfully', 'Student');


    },error =>{
      this.spinner.hide();
      return this.toastr.error('Oops Error On The Server', 'Error');



    })

    
      
  }
getStudents(){
  this.svc.getStudent().subscribe(x =>{
    this.students = x;
    console.log(x);
  });
}
getStudentsByID(){
  this.svc.getStudentById(localStorage.getItem('ID')).subscribe(x =>{
    this.loadDataFromDatabase(x);
    console.log(x);
  });
}
loadDataFromDatabase(provider: IRegister) {
  console.log(provider);
  this.myForm.patchValue({
                            id: provider.id,
                            name: provider.name,
                            dob: provider.dob,
                            gender: provider.gender,
                            grade: provider.grade,
                            schoolCode: provider.schoolCode,
                            schoolName: provider.schoolName
                          
  });
}

}

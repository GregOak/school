import { Component, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { RegisterService } from '../data/register.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { IRegister } from '../data/imodel';
import { UpdateStudentComponent } from '../update-student/update-student.component';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  providers:[NgbActiveModal]
})
export class RegisterComponent implements OnInit {
  closeResult: any;
  myForm: FormGroup;
  students:IRegister[];
  codes: any[];
  names: any[];
  grades: any[];


  
  constructor(public activeModal: NgbActiveModal,private modlService: NgbModal,private formBuilder: FormBuilder,private toastr: ToastrService,
    private svc:RegisterService,private spinner: NgxSpinnerService
) { }

  ngOnInit() {
    this.createForm();
    this.getStudents();
    this.getSchoolCodes();
    this.getSchoolNames();
    this.getGrades();
  }
  open(content) {
    this.modlService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  createForm() {
    this.myForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      dob: ['', [Validators.required, ]],
      gender: ['', [Validators.required, ]],
      grade: ['', [Validators.required, ]],
      schoolCode: ['', [Validators.required]],
      schoolName: ['', [Validators.required]],
    
    });

  }
  submitForm(){

    if (!this.myForm.valid) {
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
    }, 5000);
      return this.toastr.error('Please Fill In The Fields...', 'Error');
    }
    console.log(this.myForm.value);
    this.spinner.show();
    this.svc.saveStudent(this.myForm.value).subscribe(data =>{
      console.log(data);
      this.getStudents();
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
        this.modlService.dismissAll();
          }, 5000);
     
      return this.toastr.success('Student Details Saved Successfully', 'Student');


    },error =>{
      this.spinner.hide();
      return this.toastr.error('Oops Error On The Server', 'Error');



    })

    
      
  }
getStudents(){

  this.svc.getStudent().subscribe(x =>{
    this.students = x;
    console.log(x);
  });
}
openEdit(id) {
  localStorage.setItem('studentID',id);
  this.modlService.open(UpdateStudentComponent, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
    this.closeResult = `Closed with: ${result}`;
  }, (reason) => {
    this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  });
}
onDelete(id){
  this.spinner.show();
  this.svc.deleteStudent(id).subscribe(x =>{
    console.log(x);
    setTimeout(() =>{
      location.reload();
    },3000);
    this.modlService.dismissAll();
    this.spinner.hide();
    return this.toastr.success('Student Deleted!', 'Success!');

  })
}
getSchoolNames(){

  this.svc.getSchoolNames().subscribe(x =>{
    this.names = x;
    console.log(x);
  });
}
getSchoolCodes(){

  this.svc.getSchoolCodes().subscribe(x =>{
    this.codes = x;
    console.log(x);
  });
}
getGrades(){

  this.svc.getGrades().subscribe(x =>{
    this.grades = x;
    console.log(x);
  });
}
}

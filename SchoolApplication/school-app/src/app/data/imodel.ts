export interface IRegister {
    id: string;
    name: string;
    dob: string;
    gender: string;
    grade: string;
    schoolCode: string;
    schoolName: string;
}

import { Injectable } from '@angular/core';
import { Register } from './model';
import { IRegister } from './imodel';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';



@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  private apiUrl = environment.apiUrl;
 _register: 'Registers'
 SchoolNames
 SchoolCodes


  constructor(private httpClient:HttpClient) { }

  saveStudent(data:IRegister){
   return this.httpClient.post<IRegister>(this.apiUrl + 'Registers',data);

  }
  updateStudent(id:string,data:IRegister){
    return this.httpClient.put<IRegister>(this.apiUrl + 'Registers' + id,data);
 
   }
  getStudent(){
    return this.httpClient.get<IRegister[]>(this.apiUrl + 'Registers');

  }
  getStudentById(id){
    return this.httpClient.get<IRegister>(this.apiUrl + 'Registers/'+ id);

  }
  deleteStudent(id){
    return this.httpClient.delete(this.apiUrl + 'Registers/'+ id);

  }
  getSchoolNames(){
    return this.httpClient.get<any[]>(this.apiUrl + 'SchoolNames');

  }
  getSchoolCodes(){
    return this.httpClient.get<any[]>(this.apiUrl + 'SchoolCodes');

  }
  getGrades(){
    return this.httpClient.get<any[]>(this.apiUrl + 'Grades');

  }
}

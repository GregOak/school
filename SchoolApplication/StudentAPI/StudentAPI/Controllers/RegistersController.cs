﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using StudentAPI.Models;

namespace StudentAPI.Controllers
{
    public class RegistersController : ApiController
    {
        private SchoolDBModel db = new SchoolDBModel();

        // GET: api/Registers
        public IQueryable<Register> GetRegisters()
        {
            return db.Registers;
        }

        // GET: api/Registers/5
        [ResponseType(typeof(Register))]
        public IHttpActionResult GetRegister(Guid id)
        {
            Register register = db.Registers.Find(id);
            if (register == null)
            {
                return NotFound();
            }

            return Ok(register);
        }

        // PUT: api/Registers/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutRegister(Guid id, Register register)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != register.Id)
            {
                return BadRequest();
            }

            db.Entry(register).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RegisterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Registers
        [ResponseType(typeof(Register))]
        public IHttpActionResult PostRegister(Register register)
        {
            register.Id = Guid.NewGuid();
            register.CreatedDate = DateTime.Now;
            register.CreatedUser = register.Name;
            register.IsActive = true;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Registers.Add(register);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (RegisterExists(register.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = register.Id }, register);
        }

        // DELETE: api/Registers/5
        [ResponseType(typeof(Register))]
        public IHttpActionResult DeleteRegister(Guid id)
        {
            Register register = db.Registers.Find(id);
            if (register == null)
            {
                return NotFound();
            }

            db.Registers.Remove(register);
            db.SaveChanges();

            return Ok(register);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RegisterExists(Guid id)
        {
            return db.Registers.Count(e => e.Id == id) > 0;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using StudentAPI.Models;

namespace StudentAPI.Controllers
{
    public class SchoolNamesController : ApiController
    {
        private SchoolDBModel db = new SchoolDBModel();

        // GET: api/SchoolNames
        public IQueryable<SchoolName> GetSchoolNames()
        {
            return db.SchoolNames;
        }

        // GET: api/SchoolNames/5
        [ResponseType(typeof(SchoolName))]
        public IHttpActionResult GetSchoolName(Guid id)
        {
            SchoolName schoolName = db.SchoolNames.Find(id);
            if (schoolName == null)
            {
                return NotFound();
            }

            return Ok(schoolName);
        }

        // PUT: api/SchoolNames/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutSchoolName(Guid id, SchoolName schoolName)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != schoolName.Id)
            {
                return BadRequest();
            }

            db.Entry(schoolName).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SchoolNameExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/SchoolNames
        [ResponseType(typeof(SchoolName))]
        public IHttpActionResult PostSchoolName(SchoolName schoolName)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.SchoolNames.Add(schoolName);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (SchoolNameExists(schoolName.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = schoolName.Id }, schoolName);
        }

        // DELETE: api/SchoolNames/5
        [ResponseType(typeof(SchoolName))]
        public IHttpActionResult DeleteSchoolName(Guid id)
        {
            SchoolName schoolName = db.SchoolNames.Find(id);
            if (schoolName == null)
            {
                return NotFound();
            }

            db.SchoolNames.Remove(schoolName);
            db.SaveChanges();

            return Ok(schoolName);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SchoolNameExists(Guid id)
        {
            return db.SchoolNames.Count(e => e.Id == id) > 0;
        }
    }
}
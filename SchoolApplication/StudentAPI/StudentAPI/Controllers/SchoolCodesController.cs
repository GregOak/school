﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using StudentAPI.Models;

namespace StudentAPI.Controllers
{
    public class SchoolCodesController : ApiController
    {
        private SchoolDBModel db = new SchoolDBModel();

        // GET: api/SchoolCodes
        public IQueryable<SchoolCode> GetSchoolCodes()
        {
            return db.SchoolCodes;
        }

        // GET: api/SchoolCodes/5
        [ResponseType(typeof(SchoolCode))]
        public IHttpActionResult GetSchoolCode(Guid id)
        {
            SchoolCode schoolCode = db.SchoolCodes.Find(id);
            if (schoolCode == null)
            {
                return NotFound();
            }

            return Ok(schoolCode);
        }

        // PUT: api/SchoolCodes/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutSchoolCode(Guid id, SchoolCode schoolCode)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != schoolCode.Id)
            {
                return BadRequest();
            }

            db.Entry(schoolCode).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SchoolCodeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/SchoolCodes
        [ResponseType(typeof(SchoolCode))]
        public IHttpActionResult PostSchoolCode(SchoolCode schoolCode)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.SchoolCodes.Add(schoolCode);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (SchoolCodeExists(schoolCode.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = schoolCode.Id }, schoolCode);
        }

        // DELETE: api/SchoolCodes/5
        [ResponseType(typeof(SchoolCode))]
        public IHttpActionResult DeleteSchoolCode(Guid id)
        {
            SchoolCode schoolCode = db.SchoolCodes.Find(id);
            if (schoolCode == null)
            {
                return NotFound();
            }

            db.SchoolCodes.Remove(schoolCode);
            db.SaveChanges();

            return Ok(schoolCode);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SchoolCodeExists(Guid id)
        {
            return db.SchoolCodes.Count(e => e.Id == id) > 0;
        }
    }
}